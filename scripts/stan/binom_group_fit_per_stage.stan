data {
  int<lower=0> I; // number of groups
  int<lower=0, upper=I> i[I];  // replicate indicator

  int N_stage;
  int stage[I];

  int S;  // total number of species
  int s[I];  // species index per replicate

  int<lower=0> N[I];  // number of total cells
  int<lower=0> y[I];  // number of specific cell type

  // int N_chromium;
  // int chrom_ver[I];
  // int chrom_ver_fac[I];
}

parameters {
  matrix<lower=0, upper=1>[S,N_stage] alpha0;
  vector<lower=0, upper=1>[I] alpha;
  vector<lower=0>[S] alpha_sig;

  // real chrom_influence;
}


model {
  alpha_sig ~ exponential(1);

  for ( i_s in 1:S ) {
    for ( i_stage in 1:N_stage) {
      alpha0[i_s, i_stage] ~ student_t(1,1.5, 1);
    }
  }

  for (ii in 1:I) {

    alpha[ii] ~ normal(alpha0[s[ii], stage[ii]],
                       alpha_sig[s[ii]]);

  }

  y[i] ~ binomial(
    N[i],
    alpha[i]
  );
}
