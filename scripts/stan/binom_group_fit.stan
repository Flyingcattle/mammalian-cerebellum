data {
    int<lower=0> G; // number of groups
    int<lower=0,upper=G> x[G]; // group indicator
    int<lower=0> N[G]; // number of observations
    int<lower=0> y[G]; // number of successes
}

parameters {
    real alpha0; // prior
    /* vector[G] alpha; // per group alpha */
    real alpha[G]; // per group alpha
    real<lower=0> alpha_sig;
}

transformed parameters {
    real theta0 = inv_logit(alpha0); // prob. of success
    /* vector[G] theta = inv_logit(alpha); */
    real theta[G];
    theta[x] = inv_logit(alpha[x]);
}

model {
    /* alpha0 ~ normal(0, 1.5); */
    /* alpha_sig ~ uniform(0, 1); */
    /* alpha_sig ~ gamma(1,1); */
    alpha ~ normal(alpha0, alpha_sig);
    y[x] ~ binomial_logit(N[x], alpha[x]); // fitting using the logit of theta
}
