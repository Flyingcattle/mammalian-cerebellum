suppressPackageStartupMessages({
  library(tidyverse)
  library(ggupset)
})
theme_set(theme_classic())

species <- c(Human = 'HUM',
             Mouse = 'MOU',
             Opossum = 'OPO')

min_enrichment_x <- 2
min_freq <- 0.01

df <- read_csv('data/csv/markers_species_grouped.csv') %>% 
  dplyr::rename(dev_state = cluster)

annots <- read_tsv('data/metadata/cell_type_annotations.csv') %>% 
  select(origin, cell_type, dev_state) %>% 
  distinct()

df <- df %>% 
  left_join(annots) 

df <- df %>% 
  filter(species == 'Human/Mouse/Opossum')


df %>% 
  select(species, ortho_name, dev_state) %>% 
  distinct() %>% 
  group_by(species, ortho_name) %>% 
  summarise(dev_states = paste0(unique(dev_state) %>% sort(), collapse = '/')) %>% 
  group_by(species, dev_states) %>% 
  dplyr::count() %>% 
  arrange(species, desc(n))

df %>% 
  select(ortho_name, dev_state) %>% 
  distinct() %>% 
  group_by(ortho_name) %>% 
  summarise(dev_states = list(unique(dev_state))) %>% 
  rowwise() %>% 
  mutate(l = length(dev_states),
         d = paste0(dev_states, collapse = '/')) %>% 
  filter(l > 1) %>% 
  group_by(d) %>%
  mutate(n = n()) %>% 
  arrange(desc(n)) %>% 
  filter(n >= 5) %>%
  ggplot(aes(x = dev_states)) +
  geom_bar() +
  scale_x_upset() +
  ggsave('figures/final/conserved_marker_genes_shared_devState.pdf',
         width = 7, height = 5, useDingbats = FALSE)