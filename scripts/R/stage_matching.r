library(tidyverse)
library(patchwork)
suppressMessages(source("scripts/R/sce_helper.R"))

suppressMessages(library(ComplexHeatmap))
source("scripts/R/figures/stage_matching_functions.R")

color_list <- readRDS("data/metadata/color_list.rds")

scs = readRDS("data/sce/full_orthologs_sce.rds")
scs <- lapply(scs,
              function(x){
                 x[,!str_detect(x$Tissue, ' dn$')]
              })

mrgd = readRDS("data/sce/mouHumOpo_sce.rds")

pseudostage_assignments <- read_csv('data/csv/mouHumOpo_MOUpseudotime.csv')

stage_pbs <- lapply(scs, sce.normed.pb, group.by="stage.ord", subsample_to=NULL)

color_list$MOU

# for(species in names(stage_pbs)){
#     plot_corplot(
#         stage_pbs[[species]],
#         stage_pbs[[species]],
#         x_color = color_list[[species]],
#         y_color = color_list[[species]],
#         row_title=species,
#         column_title=species
#     ) %>% print()
# }

cat(
    " Human & Mouse:", length(intersect(rownames(stage_pbs$MOU),
                                      rownames(stage_pbs$HUM))), "\n",
    "Mouse & Opossum:", length(intersect(rownames(stage_pbs$MOU),
                                      rownames(stage_pbs$OPO))),"\n",
    "Human & Opossum:", length(intersect(rownames(stage_pbs$OPO),
                                      rownames(stage_pbs$HUM)))
)

## plot_corplot(
##     stage_pbs$MOU,
##     stage_pbs$HUM,
##     color_list$MOU,
##     color_list$HUM,
##     row_title="Mouse",
##     column_title="Human"
## )

## plot_corplot(
##     stage_pbs$MOU,
##     stage_pbs$OPO,
##     color_list$MOU,
##     color_list$OPO,
##     row_title="Mouse",
##     column_title="Opossum"
## )

## plot_corplot(
##     stage_pbs$HUM,
##     stage_pbs$OPO,
##     color_list$HUM,
##     color_list$OPO,
##     row_title="Human",
##     column_title="Opossum"
## )

hvg <- lapply(scs, function(x)get.info.genes(assay(x, "umi")))

## cat(

##     " Human & Mouse:", length(intersect(hvg$HUM, hvg$MOU)), "\n",
##     "Opossum & Mouse:", length(intersect(hvg$OPO, hvg$MOU)), "\n",
##     "Opossum & Human:", length(intersect(hvg$OPO, hvg$HUM)), "\n"

## )

## plot_corplot(
##     stage_pbs$MOU,
##     stage_pbs$HUM,
##     color_list$MOU,
##     color_list$HUM,
##     row_title="Mouse",
##     column_title="Human",
##     hvg = intersect(hvg$HUM, hvg$MOU)
## )

## plot_corplot(
##     stage_pbs$MOU,
##     stage_pbs$OPO,
##     color_list$MOU,
##     color_list$OPO,
##     row_title="Mouse",
##     column_title="Opossum",
##     hvg = intersect(hvg$OPO, hvg$MOU)
## )

## plot_corplot(
##     stage_pbs$HUM,
##     stage_pbs$OPO,
##     color_list$HUM,
##     color_list$OPO,
##     row_title="Human",
##     column_title="Opossum",
##     hvg = intersect(hvg$HUM, hvg$OPO)
## )

plot_dtw_from_matrix <- function(cdst, main="", xlab="", ylab="", 
                                 legend.lab = '\nCorrelation distance'){
    aln <- dtw::dtw(cdst)
    
    
    rownames(cdst) <- stringr::str_remove(rownames(cdst), "^.*_")
    colnames(cdst) <- stringr::str_remove(colnames(cdst), "^.*_")
    
    
    fields::image.plot(1:nrow(cdst), 1:ncol(cdst), cdst, axes=F, xlab = "", ylab="", main=main,
                       col = rev(viridis::inferno(100)),
                       legend.lab = legend.lab)
    mtext(text = rownames(cdst), side=1, at = seq(1,nrow(cdst),1), las=2, cex=.6, line=.3)
    mtext(text = colnames(cdst), side=2, at = seq(1,ncol(cdst),1), las=1, cex=.6, line=.3)
    
    mtext(text = xlab, side=1, las=1, line=3.25, cex=.8)
    mtext(text = ylab, side=2, las=3, line=3, cex=.8)
    
    lines(aln$index1, aln$index2)
}

plot_dtw <- function(x, y, hvg=intersect(rownames(x), rownames(y)), main="",
                     xlab="", ylab=""){
    cdst <- acos(custom_cor(x, y, hvg=hvg))
    #aln <- dtw::dtw(cdst, open.end=T, open.begin=T, step=asymmetric)
    
    plot_dtw_from_matrix(cdst, main=main, xlab=xlab, ylab=ylab)
}



# correlation distance --------------------------------------------------------------------------------
pdf('figures/final/stage_alignment_DTW.pdf',
    width = 8,
    height = 8)

par(mfrow=c(2,2))
print(plot_dtw(stage_pbs$MOU, stage_pbs$HUM, hvg=intersect(hvg$HUM, hvg$MOU),
         main="Human vs Mouse", xlab="Mouse", ylab="Human"))
print(plot_dtw(stage_pbs$MOU, stage_pbs$OPO, hvg=intersect(hvg$OPO, hvg$MOU),
         main="Opossum vs Mouse", xlab="Mouse", ylab="Opossum"))
print(plot_dtw(stage_pbs$OPO, stage_pbs$HUM, hvg=intersect(hvg$HUM, hvg$OPO),
         main="Opossum vs Human", xlab="Opossum", ylab="Human"))

dev.off()
par(mfrow=c(1,1))

# 
# 
# hum_correspondences <- apply(custom_cor(stage_pbs$MOU, 
#                  stage_pbs$HUM,
#                  hvg=union(hvg$MOU, 
#                            hvg$HUM)), 
#       2, 
#       function(x)names(x)[x==max(x)]) %>% 
#       as.data.frame() %>% 
#       rownames_to_column("Human Stage") %>% 
#       dplyr::rename(`Mouse [Spearman]` = 2) %>% 
#       mutate(`Human Stage` = str_remove(`Human Stage`, "^.*_")) %>% 
#       mutate(`Mouse [Spearman]` = str_remove(`Mouse [Spearman]`, "^.*_"))      
#                              
# hum_correspondences
# 
# opo_correspondences <- apply(custom_cor(stage_pbs$MOU, 
#                  stage_pbs$OPO,
#                  hvg=union(hvg$MOU, 
#                            hvg$OPO)), 
#       2, 
#       function(x)names(x)[x==max(x)]) %>% 
#       as.data.frame() %>% 
#       rownames_to_column("Opossum Stage") %>% 
#       dplyr::rename(`Mouse [Spearman]` = 2) %>% 
#       mutate(`Opossum Stage` = str_remove(`Opossum Stage`, "^.*_")) %>% 
#       mutate(`Mouse [Spearman]` = str_remove(`Mouse [Spearman]`, "^.*_"))      
#                              
# opo_correspondences
# 
# mou_corresp <- hum_correspondences %>% 
#     dplyr::rename("Stage" = `Human Stage`,
#                   "mou_crsp" = `Mouse [Spearman]`) %>% 
#     add_column(species = "Human") %>% 
#     rbind(
#         opo_correspondences %>% dplyr::rename("Stage" = `Opossum Stage`,
#                   "mou_crsp" = `Mouse [Spearman]`) %>% 
#         add_column(species="Opossum")
#     ) %>% 
#     rbind(
#         tibble(
#             Stage = sort(unique(scs$MOU$Stage)),
#             mou_crsp = sort(unique(scs$MOU$Stage)),
#             species = "Mouse"
#         )
#     )
# 
# mou_corresp
# 
# mrgd <- sce.split.by.col(mrgd, column = "species")
# 
# mrgd$HUM
# 
# hum_pseudostage <- sce.get.pseudostage(
#     x = mrgd$MOU,
#     y = mrgd$HUM,
#     x.stages = as.numeric(as.factor(mrgd$MOU$stage.ord))
# )
# 
# opo_pseudostage <- sce.get.pseudostage(
#     x = mrgd$MOU,
#     y = mrgd$OPO,
#     x.stages = as.numeric(as.factor(mrgd$MOU$stage.ord))
# )
# 
# mou_pseudostage <- sce.get.pseudostage(
#     x = mrgd$MOU,
#     y = mrgd$MOU,
#     x.stages = as.numeric(as.factor(mrgd$MOU$stage.ord))
# )
# 
# pseudostage_assignments <- list(
#     MOU = mou_pseudostage,
#     HUM = hum_pseudostage,
#     OPO = opo_pseudostage
# )
# 
# for(species in names(scs)){
#     scs[[species]] <- scs[[species]][,colnames(scs[[species]]) %in% names(pseudostage_assignments[[species]])]
#     scs[[species]]$pseudostage <- pseudostage_assignments[[species]][colnames(scs[[species]])]
#     
#     mrgd[[species]]$pseudostage <- pseudostage_assignments[[species]][colnames(mrgd[[species]])]
# }
# 
# options(repr.plot.width=15, repr.plot.height=11, repr.plot.res=300)
# 
# species_full <- c(MOU = "Mouse", HUM = "Human", OPO = "Opossum")
# 
# lapply(scs, function(s){
#     df <- sce.colData(s)
#     
#     ggplot(df, aes(UMAP1, UMAP2)) +
#     geom_point(aes(color = pseudostage), size=.05) +
#     scale_color_gradientn(colors = rev(viridis::inferno(100))) +
#     labs(
#         title = species_full[unique(df$species)],
#         x = "UMAP1",
#         y = "UMAP2"
#     )
# }) %>% wrap_plots(ncol=2, guides = "collect")
# 
# options(repr.plot.width=15, repr.plot.height=11, repr.plot.res=300)
# 
# species_full <- c(MOU = "Mouse", HUM = "Human", OPO = "Opossum")
# 
# lapply(mrgd, function(s){
#     df <- sce.colData(s)
#     df$UMAP1 <- reducedDim(s, "umap2d")[,1]
#     df$UMAP2 <- reducedDim(s, "umap2d")[,2]
#     
#     df %>% 
#     select(cell_id, species, UMAP1, UMAP2, cell_type)
# }) %>% 
#     do.call(rbind,.) %>% 
#     mutate(species = species_full[species]) %>% 
#     ggplot(aes(UMAP1, UMAP2)) +
#         geom_point(aes(color = cell_type), size=.05) +
#         scale_color_manual(values = color_list$cell_type) +
#         labs(
#             x = "UMAP1",
#             y = "UMAP2"
#         ) +
#         facet_wrap(vars(species), ncol=2)
# 
# options(repr.plot.width=15, repr.plot.height=11, repr.plot.res=300)
# 
# 
# lapply(mrgd, function(s){
#     df <- sce.colData(s)
#     df$UMAP1 <- reducedDim(s, "umap2d")[,1]
#     df$UMAP2 <- reducedDim(s, "umap2d")[,2]
#     
#     ggplot(df, aes(UMAP1, UMAP2)) +
#     geom_point(aes(color = pseudostage), size=.05) +
#     scale_color_gradientn(colors = rev(viridis::inferno(100))) +
#     labs(
#         title = species_full[unique(df$species)],
#         x = "UMAP1",
#         y = "UMAP2"
#     )
# }) %>% wrap_plots(ncol=2, guides = "collect")
# 
# pseudostage distance ---------------------------------------------------------
species_full <- c(MOU = "Mouse", HUM = "Human", OPO = "Opossum")
df_ps_colnames <- scs %>% map(function(x) {colnames(sce.colData(x))}) %>% purrr::reduce(intersect)
# 
md <- scs %>% map_dfr(function(x){ sce.colData(x)[,df_ps_colnames]})
# 
md <- md %>%
    mutate(species = species_full[species])

md <- md %>% 
    left_join(pseudostage_assignments %>% select(cell_id, pseudostage))

head(md)

x <- md %>%
    mutate(pt = cut(pseudostage, 50)) %>% 
    group_by(species, stage.ord, pt) %>%
    dplyr::count() %>%
    group_by(species, stage.ord) %>%
    mutate(rel = n / sum(n)) %>%
    select(species, stage.ord, pt, rel) %>%
    spread(pt, rel, fill=0)
# 
xy_dist <- function(x, y, method="manhattan"){
    #rownames(x) <- paste0("x", "//", rownames(x))
    #rownames(y) <- paste0("y", "//", rownames(y))

    z <- rbind(x, y)

    dst <- dist(z, method=method)

    dst <- as.matrix(dst)

    dst <- dst[rownames(x), rownames(y)]

    rownames(dst) <- stringr::str_remove(rownames(dst), "^x//")
    colnames(dst) <- stringr::str_remove(colnames(dst), "^y//")

    return(dst)
}
# 
# options(repr.plot.height=5, repr.plot.width=10)
# par(mfrow=c(1,2))
# 
pdf('figures/final/DTW_pseudostage.pdf',
    width = 5, height = 4)
xy_dist(
    x %>% filter(species == "Human") %>% as.data.frame() %>%
    select(-species) %>%
    column_to_rownames("stage.ord") %>%
    as.matrix(),

    x %>% filter(species == "Mouse") %>% as.data.frame() %>%
    select(-species) %>%
    column_to_rownames("stage.ord") %>%
    as.matrix()
) %>% t() %>%
plot_dtw_from_matrix(main = "PT dist.: Human vs. Mouse",
                     xlab = "Mouse", ylab="Human")

xy_dist(
    x %>% filter(species == "Opossum") %>% as.data.frame() %>%
    select(-species) %>%
    column_to_rownames("stage.ord") %>%
    as.matrix(),

    x %>% filter(species == "Mouse") %>% as.data.frame() %>%
    select(-species) %>%
    column_to_rownames("stage.ord") %>%
    as.matrix()
) %>% t() %>%
plot_dtw_from_matrix(main = "PT dist.: Opossum vs. Mouse",
                     xlab = "Mouse", ylab="Opossum")
dev.off()
# 
# par(mfrow=c(1,1))
# 
# 

# dev_state distance -----------------------------------------------------------
df_abund <- md %>%
    group_by(species, stage.ord, dev_state) %>%
    dplyr::count() %>%
    group_by(species, stage.ord) %>%
    mutate(n = n/sum(n)) %>%
    spread(dev_state, n, fill=0)

df_abund <- df_abund %>%
    group_by(species)

df_abund_list <- group_split(df_abund) %>%
    map(function(x){x %>%
                    #mutate(Stage = paste0(species, "//", stage.ord)) %>%
                    select(-species) %>%
                    as.data.frame() %>%
                    column_to_rownames("stage.ord") %>%
                    as.matrix()})

names(df_abund_list) <- df_abund %>% group_keys() %>% pull(1)

pdf('figures/final/DTW_devState.pdf',
    width = 5, height = 4)
xy_dist(df_abund_list$Mouse,
        df_abund_list$Human, 
        method = 'manhattan') %>% 
    plot_dtw_from_matrix(main = "dev_state Distnace: Human vs. Mouse",
                         xlab = "Mouse", ylab="Human", 
                         legend.lab = 'Manhattan distance')

xy_dist(df_abund_list$Mouse,
        df_abund_list$Opossum, 
        method = 'manhattan') %>% 
    plot_dtw_from_matrix(main = "dev_state Distnace: Opossum vs. Mouse",
                         xlab = "Mouse", ylab="Human", 
                         legend.lab = 'Manhattan distance')
dev.off()
# 
# options(repr.plot.width=6, repr.plot.height=5, repr.plot.res=300)
# 
# ctdist_hum_mou <- xy_dist(df_abund_list$Human,
#         df_abund_list$Mouse) 
# 
# ctdist_hum_mou %>% 
# ComplexHeatmap::Heatmap(cluster_rows = F, 
#                         cluster_columns = F, 
#                         col = viridis::inferno(100),
#                         row_title = "Human",
#                         column_title = "Mouse",
#                         heatmap_legend_param = list(title="Distance"))
# 
# options(repr.plot.width=6, repr.plot.height=5, repr.plot.res=300)
# 
# ctdist_opo_mou <- xy_dist(df_abund_list$Opossum,
#         df_abund_list$Mouse) 
# 
# ctdist_opo_mou %>% 
# ComplexHeatmap::Heatmap(cluster_rows = F, 
#                         cluster_columns = F, 
#                         col = viridis::inferno(100),
#                         row_title = "Opossum",
#                         column_title = "Mouse",
#                         heatmap_legend_param = list(title="Distance"))
# 
# options(repr.plot.height=5, repr.plot.width=10)
# par(mfrow=c(1,2))
# 
# plot_dtw_from_matrix(ctdist_hum_mou, main="dev_state dist.: Human vs. Mouse",
#                      xlab = "Human", ylab="Mouse")
# plot_dtw_from_matrix(ctdist_opo_mou, main="dev_state dist.: Opossum vs. Mouse",
#                      xlab = "Opossum", ylab="Mouse")
# 
# par(mfrow=c(1,1))
# 
# ctdist_hm_df <- ctdist_hum_mou %>% 
#     as.data.frame() %>% 
#     rownames_to_column("Human stage") %>% 
#     gather("Mouse stage", "dst", -`Human stage`) %>% 
#     group_by(`Human stage`) %>% 
#     filter(dst == min(dst)) %>% 
#     ungroup() 
# 
# ctdist_hm_df %>% 
#     mutate(`Human stage`=str_remove(`Human stage`, "^.*_")) %>% 
#     group_by(`Mouse stage`) %>% 
#     summarise(`Human stage` = paste0(`Human stage`, collapse="/")) %>% 
#     ungroup() %>% 
#     mutate(`Mouse stage` = factor(str_remove(`Mouse stage`, "^.*_"),
#                                   levels = str_remove(`Mouse stage`, "^.*_")))
# 
# ctdist_om_df <- ctdist_opo_mou %>% 
#     as.data.frame() %>% 
#     rownames_to_column("Opossum stage") %>% 
#     gather("Mouse stage", "dst", -`Opossum stage`) %>% 
#     group_by(`Opossum stage`) %>% 
#     filter(dst == min(dst)) %>% 
#     ungroup() 
# 
# ctdist_om_df %>% 
#     mutate(`Opossum stage`=str_remove(`Opossum stage`, "^.*_")) %>% 
#     group_by(`Mouse stage`) %>% 
#     summarise(`Opossum stage` = paste0(`Opossum stage`, collapse="/")) %>% 
#     ungroup() %>% 
#     mutate(`Mouse stage` = factor(str_remove(`Mouse stage`, "^.*_"),
#                                   levels = str_remove(`Mouse stage`, "^.*_")))
# 
# ctdist_hm_df %>% 
#     select(`Human stage`, `Mouse stage`) %>% 
#     full_join(
#         ctdist_om_df %>% 
#             select(`Opossum stage`, `Mouse stage`)
#     ) %>% 
#     select(`Mouse stage`, `Human stage`, `Opossum stage`) %>% 
#     replace_na(`Opossum stage` = "", 
#                `Human stage` = "") %>% 
#     mutate(
#         `Human stage` = str_remove(`Human stage`, "^.*_"),
#         `Opossum stage`= str_remove(`Opossum stage`, "^.*_")
#     ) %>% 
#     group_by(`Mouse stage`) %>% 
#     summarise(
#         `Human stage` = paste0(unique(`Human stage`), collapse="/"),
#         `Opossum stage` = paste0(unique(`Opossum stage`), collapse="/")
#     )
# 
# colvec <- readRDS("../data/metadata/color_list.rds")
# 
# ct_lvls <- c( "astroglia", "oligodendrocyte", "ependymal", "VZ_neuroblast", "parabrachial", 
#              "noradrenergic", "GABA_DN",  "Purkinje", "interneuron", "NTZ_neuroblast", "NTZ_mixed", "isth_N", 
#              "glut_DN", "GC", "UBC", "GC/UBC", "meningeal", "neural_crest_progenitor", "isthmic_neuroblast", 
#              "MB_neuroblast", "GABA_MB", "MBO", "motorneuron", "erythroid", "immune", "mural/endoth", "oligo")
# 
# options(repr.plot.width=10, repr.plot.height=10, repr.plot.res=300)
# 
# md %>% 
#     filter(!is.na(cell_type)) %>% 
#     group_by(`HUM corr. stage`, cell_type, species) %>% 
#     filter(n() > 25) %>% 
#     summarise(n = n()) %>% 
#     group_by(species, `HUM corr. stage`) %>% 
#     mutate(rel = n/sum(n)) %>% 
#     mutate(cell_type = factor(cell_type, levels=ct_lvls)) %>% 
#     ggplot(aes(x = `HUM corr. stage`, y = rel, fill = cell_type)) +
#     geom_bar(stat="identity") +
#     facet_grid(rows=vars(species)) +
#     scale_fill_manual(values = colvec$cell_type) +
#     scale_x_discrete(labels = function(x){str_remove(x, "^.*_")}) +
#     theme(axis.text.x=element_text(angle=45, hjust=1, vjust=1)) +
#     labs(
#         title = "Human corresponding stage",
#         y = "Relative abundance"
#     )
# 
# options(repr.plot.width=10, repr.plot.height=10, repr.plot.res=300)
# 
# md %>% 
#     left_join(mou_corresp) %>% 
#     filter(!is.na(cell_type)) %>% 
#     group_by(mou_crsp, cell_type, species) %>% 
#     filter(n() > 25) %>% 
#     summarise(n = n()) %>% 
#     group_by(species, mou_crsp) %>% 
#     mutate(rel = n/sum(n)) %>% 
#     mutate(cell_type = factor(cell_type, levels=ct_lvls)) %>% 
#     ungroup() %>% 
#     mutate(mou_crsp = factor(mou_crsp, levels=c(
#         "E10.5", "E11.5", "E12.5", "E13.5",
#         "E14.5", "E15.5", "E17.5", "P0", "P4",
#         "P7", "P14", "adult"
#     ))) %>% 
#     ggplot(aes(x = mou_crsp, y = rel, fill = cell_type)) +
#     geom_bar(stat="identity") +
#     facet_grid(rows=vars(species)) +
#     scale_fill_manual(values = colvec$cell_type) +
#     scale_x_discrete(labels = function(x){str_remove(x, "^.*_")}) +
#     theme(axis.text.x=element_text(angle=45, hjust=1, vjust=1)) +
#     labs(
#         title = "Correlation based correspondence",
#         y = "Relative abundance"
#     )
# 
# colnames(md)
# 
# md <- md %>% 
#     group_by(species, TissueID, `Capture System`) %>% 
#     mutate(pt = median(pseudostage)) %>% 
#     ungroup() %>% 
#     mutate(pt = round(pt)) %>% 
#     mutate(pt = as.factor(pt)) 
# 
# md %>% 
#     filter(!is.na(cell_type)) %>% 
#     group_by(pt, cell_type, species) %>% 
#     filter(n() > 25) %>% 
#     summarise(n = n()) %>% 
#     group_by(species, pt) %>% 
#     mutate(rel = n/sum(n)) %>% 
#     mutate(cell_type = factor(cell_type, levels=ct_lvls)) %>% 
#     ggplot(aes(x = pt, y = rel, fill = cell_type)) +
#     geom_bar(stat="identity") +
#     facet_grid(rows=vars(species)) +
#     scale_fill_manual(values = colvec$cell_type) +
#     scale_x_discrete(labels = function(x){str_remove(x, "^.*_")}) +
#     labs(
#         title = "Pseudostage correspondences",
#         y = "Relative abundance"
#     )
# 
# options(repr.plot.width=10, repr.plot.height=10, repr.plot.res=300)
# 
# 
# md %>% 
#     filter(cell_type %in% c("astroglia",
#                             "Purkinje",
#                             "GC",
#                             "interneuron")) %>% 
#     group_by(stage.ord, cell_type) %>% 
#     filter(n() > 50) %>% 
#     ggplot(aes(x = stage.ord, y = pseudostage)) +
#     #geom_violin(aes(fill = cell_type)) +
#     geom_boxplot(aes(fill=cell_type), outlier.size = .1, width=.75) +
#     scale_fill_manual(values = colvec$cell_type) +
#     scale_x_discrete(labels = function(x){str_remove(x, "^.*_")}) +
#     facet_grid(vars(cell_type), vars(species), scales="free_x") +
#     theme(axis.text.x = element_text(angle=45, hjust=1, vjust=1))
# 
# options(repr.plot.width=17, repr.plot.height=5, repr.plot.res=300)
# 
# 
# md %>% 
#     filter(cell_type %in% c("astroglia",
#                             "Purkinje",
#                             "GC",
#                             "interneuron")) %>% 
#     group_by(stage.ord, cell_type) %>% 
#     filter(n() > 50) %>% 
#     ggplot(aes(x = stage.ord, y = pseudostage)) +
#     #geom_violin(aes(fill = cell_type)) +
#     geom_boxplot(aes(fill=cell_type), outlier.size = .1) +
#     scale_fill_manual(values = colvec$cell_type) +
#     scale_x_discrete(labels = function(x){str_remove(x, "^.*_")}) +
#     facet_grid(cols = vars(species), scales="free_x") +
#     theme(axis.text.x = element_text(angle=45, hjust=1, vjust=1)) +
#     labs(
#         x = "Stage",
#         y = "Pseudostage",
#         fill = "Cell type"
#     )
# 
# md %>% 
#     select(Stage, `HUM corr. stage`, species) %>% 
#     distinct() %>% 
#     group_by(species, `HUM corr. stage`) %>% 
#     summarise(Stage = paste0(Stage, collapse="/")) %>% 
#     spread(species, Stage, fill="")
# 
# md %>% 
#     select(Stage, species) %>% 
#     distinct() %>% 
#     left_join(mou_corresp) %>% 
#     distinct() %>% 
#     group_by(species, mou_crsp) %>% 
#     summarise(Stage = paste0(Stage, collapse="/")) %>% 
#     spread(species, Stage, fill="") %>% 
#     mutate(mou_crsp = factor(mou_crsp, levels=c(
#         "E10.5", "E11.5", "E12.5", "E13.5",
#         "E14.5", "E15.5", "E17.5", "P0", "P4",
#         "P7", "P14", "adult"
#     ))) %>% 
#     arrange(mou_crsp) %>% 
#     dplyr::rename(
#         `Mouse correspondence` = mou_crsp
#     ) %>% 
#     select(`Mouse correspondence`, Mouse, Human, Opossum)
# 
# md %>% 
#     group_by(Stage, species, pt) %>% 
#     summarise(n = n()) %>% 
#     group_by(Stage, species) %>% 
#     filter(n == max(n)) %>% 
#     group_by(species, pt) %>% 
#     summarise(Stage = paste0(Stage, collapse="/")) %>% 
#     spread(species, Stage, fill="") %>% 
#     select(pt, Mouse, Human, Opossum)
# 
# ctdist_hm_df %>% 
#     select(`Human stage`, `Mouse stage`) %>% 
#     full_join(
#         ctdist_om_df %>% 
#             select(`Opossum stage`, `Mouse stage`)
#     ) %>% 
#     select(`Mouse stage`, `Human stage`, `Opossum stage`) %>% 
#     replace_na(`Opossum stage` = "", 
#                `Human stage` = "") %>% 
#     mutate(
#         `Human stage` = str_remove(`Human stage`, "^.*_"),
#         `Opossum stage`= str_remove(`Opossum stage`, "^.*_")
#     ) %>% 
#     group_by(`Mouse stage`) %>% 
#     summarise(
#         `Human stage` = paste0(unique(`Human stage`), collapse="/"),
#         `Opossum stage` = paste0(unique(`Opossum stage`), collapse="/")
#     )
