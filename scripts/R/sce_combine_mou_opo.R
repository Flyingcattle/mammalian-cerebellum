source("scripts/R/sce_liger.R")

mou <- readRDS("generated_data/singleCellExperimentObjects/mou_sce.rds")
opo <- readRDS("generated_data/singleCellExperimentObjects/opo_sce.rds")
mou.opo <- readRDS("generated_data/liger_output_combined/mouOpo_liger_out.rds")
mou.opo.sce <- liger.to.sce(mou.opo, list(mou,opo))
rm(mou.opo)

mnn.mou.opo <- readRDS("generated_data/liger_derived_combined/mouOpo_mnn_liger.rds")
reducedDim(mou.opo.sce, "liger") <- mnn.mou.opo$corrected[colnames(mou.opo.sce), ]

mnn.mou.opo <- sce.run.umap(mou.opo.sce, n_components = 2)

saveRDS(mnn.mou.opo, "generated_data/singleCellExperimentObjects/mouOpo_sce.rds")