library(hdf5r)
library(Matrix)
library(tidyverse)
source("https://gist.githubusercontent.com/klprint/1ab4468eb3c54abcf0422dec6223b8fc/raw/96624adfe2f611b7c6568806d54c8062420d172c/single_cell_functions.R")
library(SingleCellExperiment)

sample.annot <- read_csv("metadata/batch_annotations.csv")

cell.meta <- read_csv("generated_data/metadata/all_cell_annotations.csv") %>% 
  mutate(cell_type = ifelse(is.na(cell_type), "ND", cell_type)) %>% 
  left_join(sample.annot)


dir.create("generated_data/singleCellExperimentObjects", showWarnings = F, recursive = T)

args <- commandArgs(trailingOnly = T)
species <- args[1]

mou.ft <- readRDS(paste0("generated_data/liger_input/",species,"_umi_list.rds")) %>% do.call(merge.sparse, .)
mou.liger <- readRDS(paste0("generated_data/liger_output/",species,"_liger_out.rds"))
mou.umap <- readRDS(paste0("generated_data/liger_derived/",species,"_liger_umaps.rds"))

mou <- SingleCellExperiment(assays = list(umi = mou.ft),
                            colData = cell.meta %>% 
                              filter(cell_id %in% colnames(mou.ft)) %>% 
                              column_to_rownames("cell_id"),
                            reducedDims = list(
                              umap2d = mou.umap$umap2d,
                              umap3d = mou.umap$umap3d,
                              liger = do.call(rbind, mou.liger@H)
                            ))

mou$stage.ord <- paste0(str_extract(mou$`HUM corr. stage`, "[0-9][0-9]_"),
                        mou$Stage)
saveRDS(mou, paste0("generated_data/singleCellExperimentObjects/",species,"_sce.rds"))
