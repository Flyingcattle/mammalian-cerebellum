## Commandline arugments
## Using ~optparse~ to get the commandline arugments.


## [[file:../../org/dpt_calling.org::*Commandline arugments][Commandline arugments:1]]
library(optparse)

parser <- OptionParser()

parser <- add_option(
  parser,
  c('-r', '--iroot'),
  type = 'character',
  help = 'Root cell ID',
  metavar = 'ID',
  default = NULL
)

parser <- add_option(
  parser,
  c('-n', '--neighbors_from'),
  type = 'character',
  help = 'Root cell ID',
  metavar = 'ID',
  default = NULL
)

parser <- add_option(
  parser,
  c('-i', '--n_neighbors'),
  type = 'numeric',
  help = 'Number of neighbors to consider',
  metavar = 'N',
  default = 30
)

parser <- add_option(
  parser,
  c('-d', '--n_dcs'),
  type = 'numeric',
  help = 'Number of diffusion components to embed in',
  metavar = 'N',
  default = 10
)

args <- parse_args(parser, positional_arguments = TRUE)

n_dcs <- args$options$n_dcs
iroot <- args$options$iroot
neighbors_from <- args$options$neighbors_from
n_neighbors <- args$options$n_neighbors
## Commandline arugments:1 ends here



## Testing whether the needed options are set:


## [[file:../../org/dpt_calling.org::*Commandline arugments][Commandline arugments:2]]
if(is.null(neighbors_from) | is.null(iroot)){

  stop('Please set --neighbors_from and --iroot')

}
## Commandline arugments:2 ends here



## Parsing the positional arguments:


## [[file:../../org/dpt_calling.org::*Commandline arugments][Commandline arugments:3]]
sce_file <- args$args[1]
outfile <- args$args[2]
## Commandline arugments:3 ends here

## Helper functions
## Converting a SingleCellExperiment object to an anndata object to use in ~scanpy~:

## [[file:../../org/dpt_calling.org::*Helper functions][Helper functions:1]]
sce.to.anndata <- function(sce,
                           neighbors_from = "liger",
                           n_neighbors = 30,
                           n_pcs = ncol(reducedDim(sce, neighbors_from)),
                           assay = "umi"){

  sc <- import("scanpy")

  ad <- sc$AnnData(X = t(assay(sce, assay)),
                   obs = as.data.frame(colData(sce)),
                   obsm = reducedDims(sce)@listData[!startsWith(reducedDimNames(sce), "X_")])

  sc$pp$neighbors(ad, n_neighbors = as.integer(n_neighbors), use_rep = neighbors_from, n_pcs = as.integer(n_pcs))


  return(ad)
}
## Helper functions:1 ends here

## Main programm
## Next, I need to make sure that the active conda environment will be used by ~reticulate~:


## [[file:../../org/dpt_calling.org::*Main programm][Main programm:1]]
conda_prefix <- Sys.getenv('CONDA_PREFIX')
Sys.setenv('RETICULATE_PYTHON' = paste0(conda_prefix, '/bin/python'))

library(reticulate)
py_config()
## Main programm:1 ends here



## And loading the packages:


## [[file:../../org/dpt_calling.org::*Main programm][Main programm:2]]
suppressPackageStartupMessages({
  library(SingleCellExperiment)
  library(tidyverse)
  source('scripts/R/figures/helpers.R')
})
## Main programm:2 ends here



## Reading in the data and converting the ~SCE~ object to an ~anndata~ object.


## [[file:../../org/dpt_calling.org::*Main programm][Main programm:3]]
stopifnot(file.exists(sce_file))
sce <- readRDS(sce_file)

iroot <- read_file(iroot)
iroot <- which(colnames(sce) == iroot) -1

if(length(iroot) == 0) stop('Root cell index could not be found')

iroot <- as.integer(iroot)
n_dcs <- as.integer(n_dcs)

ad <- sce.to.anndata(sce,
                     neighbors_from = neighbors_from,
                     n_neighbors = n_neighbors)
## Main programm:3 ends here



## Now, I call this script using ~reticulate~


## [[file:../../org/dpt_calling.org::*Running DPT][Running DPT:2]]
py_run_file('scripts/Python/reticulate_calculate_dpt.py')
## Running DPT:2 ends here

## Creating the output

## After running the diffusion map and DPT, I create the output table


## [[file:../../org/dpt_calling.org::*Creating the output][Creating the output:1]]
out <- tibble(
  cell_id = colnames(sce),
  dpt = ad$obs['dpt_pseudotime'][,1]
)

diffmap <- ad$obsm['X_diffmap'] %>%
  as.data.frame() %>%
  as_tibble(.) %>%
  add_column(cell_id = colnames(sce))

out <- left_join(out, diffmap, by = 'cell_id')
## Creating the output:1 ends here



## And saving the table to the predefined location


## [[file:../../org/dpt_calling.org::*Creating the output][Creating the output:2]]
write_csv(out, outfile)
## Creating the output:2 ends here
