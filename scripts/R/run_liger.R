library(optparse)

opt.list = list(
  make_option(
    "--kLiger",
    type = "numeric",
    default = 100,
    help = "Number of LIGER factors to be generated"
  ),
  make_option(
    "--resolution",
    type="numeric",
    default = 3,
    help = "Louvain cluster resolution"
  ),
  make_option(
    "--clusterThresh",
    type="numeric",
    default = 50,
    help = "Small cluster threshold"
  )
)

opts = parse_args(
  OptionParser(
    option_list = opt.list,
    usage = "Rscript tun_liger.R [options] input output",
    description = "Running liger on collected data"
  ),
  positional_arguments = T
)

library(hdf5r)
library(Matrix)
library(liger)

# umi = read_merged_data(opts$args[1])
umi <- readRDS(opts$args[1])

cat("Creating LIGER object\n")
l = createLiger(umi)

cat("Preprocessing LIGER\n")
l = normalize(l)
l = selectGenes(l)
l = scaleNotCenter(l)


cat("Running LIGER\n")
l = optimizeALS(l, k = opts$options$kLiger)
# l = quantileAlignSNF(l, resolution = opts$options$resolution,
#                      opts$options$clusterThresh)

cat("Saving the processed LIGER object to ", opts$args[2], "\n")
saveRDS(l, opts$args[2])

