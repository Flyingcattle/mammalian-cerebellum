library(tidyverse)
suppressMessages(library(SingleCellExperiment))

species <- c(
    Human = "hum",
    Mouse = "mou",
    Opossum = "opo"
)


bio_unit <- "dev_state"

# reading the TF-IDF output
tfidf_out <- lapply(species, function(s){
    read_csv(glue::glue("data/csv/gene_enrichments/{s}_tfidf_celltype_match.csv"))
})
shared_tested_clusters <- lapply(tfidf_out, function(x) x$cluster) %>%
  purrr::reduce(intersect)

sce <- lapply(species, function(s){
    readRDS(glue::glue("data/sce/updated/{s}_sce.rds"))
})

cell_metadata <- lapply(sce, function(x){
  tmp <- colData(x) %>%
    as.data.frame() %>%
    rownames_to_column("cell_id")

  tmp <- tmp[,c("cell_id", "cell_type", "dev_state")]

  as_tibble(tmp)
}) %>%
  do.call(rbind,.)

ct_dev <- cell_metadata %>%
  group_by(cell_type, dev_state) %>%
  distinct()


do_combine <- function(x, gr){
    tapply(1:ncol(x), gr, function(i){
        rowSums(x[,i,drop=F])
    }) %>%
    do.call(cbind, .)
}

do_combine_prop <- function(x, gr){
    tapply(1:ncol(x), gr, function(i){
        rowSums(x[,i,drop=F]) / length(i)
    }) %>%
    do.call(cbind, .)
}

pb <- lapply(sce, function(s){
    s <- s[,s[[bio_unit]] %in% shared_tested_clusters]
    x <- assay(s, "umi")
    ## x <- x[,s[[bio_unit]] %in% shared_tested_clusters]
    ## gr <- s$cell_type
    gr <- s[[bio_unit]]
    xi <- x > 0

    umi <- do_combine(x, gr)
    exprs <- t(t(umi) / colSums(umi)) * 1e6

    list(
        umi = umi,
        cpm = exprs,
        prop = do_combine_prop(xi, gr)
    )
})


# reading the gene metadata
# for this analysis, we only consider
# one-to-one-to-one orthologs
gmd <- read_csv("data/metadata/all_genes_meta.csv") %>%
    filter(ortho_type == "ortholog_one2one")
gene_to_ortho <- gmd %>%
    select(gene_id, ortho_name) %>%
    distinct()

# subsetting the TF-IDF output
# for one-to-one orthologs
o2o <- tfidf_out %>%
    map(function(x){
        x %>%
            left_join(gmd, by = c("gene_id")) %>%
            filter(!is.na(ortho_type))
    })

shared_genes <- o2o %>%
    map(function(x) x %>% pull(ortho_id)) %>%
    purrr::reduce(intersect)
length(shared_genes)

o2o <- o2o %>%
    map(function(x){
        x %>%
            filter(ortho_id %in% shared_genes)
    }) %>%
    do.call(rbind, .)

# using the top scoring
# TF-IDF genes in all species
min_enrichment_x <- 2
min_freq <- 0.01
shared_markers <- o2o %>%
  filter(geneFrequency >= min_freq) %>%
  mutate(enr = geneFrequency / geneFrequencyOutsideCluster) %>%
  filter(enr > min_enrichment_x) %>%
    select(species, ortho_name, tfidf, cluster) %>%
  spread(species, tfidf) %>%
  drop_na() %>%
  group_by(cluster) %>%
  mutate(HUM = HUM / max(HUM),
         MOU = MOU / max(MOU),
         OPO = OPO / max(OPO)) %>%
  ungroup() %>%
    mutate(score = sqrt(HUM^2 + MOU^2 + OPO^2)) %>%
    arrange(desc(score)) %>%
    arrange(cluster, desc(score))

write_csv(shared_markers, path = "data/csv/conserved_markers_celltypes.csv")

top_markers <- shared_markers %>%
    group_by(cluster) %>%
    top_n(5, score) %>%
    left_join(
        gmd %>%
            select(ortho_name, gene_name, gene_id)
    )


min_max_scaler <- function(x){
    ## x <- x - min(x)
    return(x / max(x))
}

exprs_df <- lapply(names(pb), function(n){
    p <- pb[[n]]
    genes <- top_markers$gene_id
    x <- p$cpm
    x <- x[rownames(x) %in% genes, ,drop=F]
    x <- t(apply(x, 1, min_max_scaler))

    rownames(x) <- gene_to_ortho$ortho_name[match(rownames(x), gene_to_ortho$gene_id)]

    x <- x %>%
        as.data.frame() %>%
        rownames_to_column("ortho_name") %>%
        as_tibble() %>%
        gather("cluster", "exprs", -ortho_name) %>%
        add_column(species = n)

    return(x)
}) %>%
    do.call(rbind,.)

prop_df <- lapply(names(pb), function(n){
    p <- pb[[n]]
    genes <- top_markers$gene_id
    x <- p$prop
    x <- x[rownames(x) %in% genes, ,drop=F]

    rownames(x) <- gene_to_ortho$ortho_name[match(rownames(x), gene_to_ortho$gene_id)]

    x <- x %>%
        as.data.frame() %>%
        rownames_to_column("ortho_name") %>%
        as_tibble() %>%
        gather("cluster", "prop", -ortho_name) %>%
        add_column(species = n)

    return(x)
}) %>%
    do.call(rbind,.)

use_celltypes <- c(
    "astroglia",
    "GC",
    "interneuron",
    "Purkinje",
    "UBC"
)
dev_state_order <- c("astrocyte", "glioblast", "progenitor",
                     "GCP", "GC", "interneuron",
                     "Purkinje", "UBC")


use_celltypes <- ct_dev %>%
  select(cell_type, dev_state) %>%
  distinct() %>%
  filter(cell_type %in% use_celltypes) %>%
  pull(dev_state)
use_celltypes <- use_celltypes[!is.na(use_celltypes)]
use_celltypes <- use_celltypes[use_celltypes %in% top_markers$cluster]
if(bio_unit == "dev_state"){
    use_celltype_order <- str_split(use_celltypes, pattern = "_", simplify = TRUE) %>%
      as_tibble(.) %>%
      mutate(V2 = factor(V2, levels = c("", "diff", "defined", "mature"))) %>%
      mutate(V1 = factor(V1, levels = dev_state_order)) %>%
      arrange(V1, V2, V3) %>%
      mutate(gr = paste0(V1, "_", V2, "_", V3)) %>%
      mutate(gr = str_remove(gr, "_*$")) %>%
      pull(gr)
}else{
  use_celltype_order <- order(use_celltypes)
}

use_genes <- top_markers %>%
  ungroup() %>%
  filter(cluster %in% use_celltypes) %>%
  mutate(cluster = factor(cluster, levels = use_celltype_order)) %>%
  arrange(cluster) %>%
    pull(ortho_name) %>%
    unique()


plt <- exprs_df %>%
  filter(cluster %in% use_celltypes) %>%
    filter(ortho_name %in% use_genes) %>%
    left_join(prop_df, by = c("species", "ortho_name", "cluster")) %>%
    mutate(ortho_name = factor(ortho_name, levels = rev(use_genes))) %>%
    ggplot(aes(x = cluster,
               y = ortho_name)) +
        geom_point(aes(color = exprs, size = prop)) +
        facet_wrap(vars(species)) +
        scale_color_gradientn(
            colors = rev(viridisLite::inferno(100))
        ) +
  scale_x_discrete(limits = use_celltype_order) +
  scale_size_continuous(range = c(1,4)) +
        labs(
            x = "Cell type",
            y = "Gene [mouse ortholog name]",
            size = "Proportion\nof cells",
            color = "Scaled\nexpression"
        ) +
        theme(
            axis.text.x = element_text(angle=45, hjust=1, vjust=1),
          axis.text.y = element_text(face = "italic"),
          panel.background = element_blank(),
          panel.grid.major = element_line(size = .25,
                                          colour = "gray")
        )
ggsave("figures/final/conserved_marker_genes.pdf", plt, width=10, height=7)



#### TF MARKER GENES ####

# mouse transcription factors
# based on GO terms
tf_df <- read_csv("data/metadata/mou_hum_opo_tf_orthologs.csv")

top_markers_tf <- shared_markers %>%
  filter(ortho_name %in% tf_df$`Gene name`) %>%
  group_by(cluster) %>%
  top_n(10, score) %>%
  ungroup()

use_genes_tf <- top_markers_tf %>%
  filter(cluster %in% use_celltypes) %>%
  mutate(cluster = factor(cluster, levels = use_celltype_order)) %>%
  arrange(cluster) %>%
    pull(ortho_name) %>%
    unique()


plt <- exprs_df %>%
  filter(cluster %in% use_celltypes) %>%
    filter(ortho_name %in% use_genes_tf) %>%
    left_join(prop_df, by = c("species", "ortho_name", "cluster")) %>%
    mutate(ortho_name = factor(ortho_name, levels = rev(use_genes_tf))) %>%
    ggplot(aes(x = cluster,
               y = ortho_name)) +
        geom_point(aes(color = exprs, size = prop)) +
        facet_wrap(vars(species)) +
        scale_color_gradientn(
            colors = rev(viridisLite::inferno(100))
        ) +
  scale_x_discrete(limits = use_celltype_order) +
  scale_size_continuous(range = c(1,4)) +
        labs(
            x = "Cell type",
            y = "Gene [mouse ortholog name]",
            size = "Proportion\nof cells",
            color = "Scaled\nexpression"
        ) +
        theme(
            axis.text.x = element_text(angle=45, hjust=1, vjust=1),
          axis.text.y = element_text(face = "italic"),
          panel.background = element_blank(),
          panel.grid.major = element_line(size = .25,
                                          colour = "gray")
        )
ggsave("figures/final/conserved_TFmarker_genes.pdf", plt, width=10, height=8)

## top_markers <- o2o %>%
##     select(species, ortho_name, tfidf, cluster) %>%
##     filter(ortho_name %in% tf_df$`Gene name`) %>%
##     spread(species, tfidf, fill = 0) %>%
##     mutate(score = sqrt(HUM^2 + MOU^2 + OPO^2)) %>%
##     arrange(desc(score)) %>%
##     group_by(cluster) %>%
##     top_n(5, score) %>%
##     arrange(cluster, desc(score)) %>%
##     left_join(
##         gmd %>%
##             select(ortho_name, gene_name, gene_id)
##     )

## min_max_scaler <- function(x){
##     x <- x - min(x)
##     return(x / max(x))
## }

## exprs_df <- lapply(names(pb), function(n){
##     p <- pb[[n]]
##     genes <- top_markers$gene_id
##     x <- p$cpm
##     x <- x[rownames(x) %in% genes, ,drop=F]
##     x <- t(apply(x, 1, min_max_scaler))

##     rownames(x) <- gene_to_ortho$ortho_name[match(rownames(x), gene_to_ortho$gene_id)]

##     x <- x %>%
##         as.data.frame() %>%
##         rownames_to_column("ortho_name") %>%
##         as_tibble() %>%
##         gather("cluster", "exprs", -ortho_name) %>%
##         add_column(species = n)

##     return(x)
## }) %>%
##     do.call(rbind,.)

## prop_df <- lapply(names(pb), function(n){
##     p <- pb[[n]]
##     genes <- top_markers$gene_id
##     x <- p$prop
##     x <- x[rownames(x) %in% genes, ,drop=F]

##     rownames(x) <- gene_to_ortho$ortho_name[match(rownames(x), gene_to_ortho$gene_id)]

##     x <- x %>%
##         as.data.frame() %>%
##         rownames_to_column("ortho_name") %>%
##         as_tibble() %>%
##         gather("cluster", "prop", -ortho_name) %>%
##         add_column(species = n)

##     return(x)
## }) %>%
##     do.call(rbind,.)


## ## use_celltypes <- c(
## ##     "astroglia",
## ##     "GC",
## ##     "interneuron",
## ##     "Purkinje",
## ##     "UBC"
## ## )

## use_genes <- top_markers %>%
##     filter(cluster %in% use_celltypes) %>%
##     pull(ortho_name) %>%
##     unique()


## plt <- exprs_df %>%
##     filter(cluster %in% use_celltypes) %>%
##     filter(ortho_name %in% use_genes) %>%
##     left_join(prop_df, by = c("species", "ortho_name", "cluster")) %>%
##     mutate(ortho_name = factor(ortho_name, levels = rev(use_genes))) %>%
##     ggplot(aes(x = cluster,
##                y = ortho_name)) +
##         geom_point(aes(color = exprs, size = prop)) +
##         facet_wrap(vars(species)) +
##         scale_color_gradientn(
##             colors = rev(viridisLite::inferno(100))
##         ) +
##         labs(
##             x = "Cell type",
##             y = "Gene [mouse ortholog name]",
##             size = "Proportion\nof cells",
##             color = "Scaled\nexpression"
##         ) +
##         theme(
##             axis.text.x = element_text(angle=45, hjust=1, vjust=1),
##           axis.text.y = element_text(face = "italic"),
##           panel.background = element_blank(),
##           panel.grid.major = element_line(size = .25,
##                                           colour = "gray")

##         )

## ggsave("figures/final/conserved_TFmarker_genes.pdf", plt, width=6, height=7)
