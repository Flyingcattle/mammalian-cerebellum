library(tidyverse)
library(pheatmap)

args <- commandArgs(trailingOnly = TRUE)

## debugging:
# args <- c(
#   "data/csv/spearmans_replica_mou.csv",
#   "figures/final/figS01_b_spearmans_mou.pdf"
# )

cormat_file <- args[1]
plot_file <- args[2]

## DEPENDS ON ------------------------------------------------------------------
current_species <- str_extract(cormat_file, pattern = '_...\\.csv') %>% 
  str_remove('_') %>% 
  str_remove('\\.csv') %>% 
  toupper()

stage_colors <- read_csv('data/metadata/stage_matching_colors_long.csv') %>% 
  filter(species == current_species) %>% 
  select(Stage, Stage_colour, Match_Stage) %>% 
  arrange(Match_Stage)

cormat <- read_csv(cormat_file) %>%
  as.data.frame() %>%
  column_to_rownames("pb") %>%
  as.matrix()

cormat <- cormat[order(rownames(cormat)), order(colnames(cormat))]

annotation <- tibble(
  pb = colnames(cormat)
) %>%
  separate(
    pb,
    sep = "//",
    into = c("Stage", "BioRep", "Chromium_Version", 'batch'),
    remove = FALSE
  ) %>%
  mutate(stage_clean = str_remove(Stage, '^.._')) %>% 
  mutate(BioRep = case_when(
           str_detect(BioRep, "\\+") ~ "Pool",
           TRUE ~ BioRep
         )) %>%
  group_by(Stage) %>%
  mutate(
    BioRep = case_when(
      BioRep == "Pool" ~ "Pool",
      TRUE ~ as.character(as.numeric(as.factor(BioRep)))
    )
  ) %>%
  mutate(Chromium_Version = dplyr::recode(Chromium_Version, 
                                          Chromium = 'V2',
                                          Chromium_v3 = 'V3')) %>% 
  as.data.frame() %>%
  column_to_rownames("pb")

fixed_batches <- c('SN149', 'SN122', 'SN233', 'SN213', 'SN234', 'SN214', 'SN222', 'SN207')
fixed_order <- annotation[annotation$batch %in% fixed_batches, ]

if(nrow(fixed_order) > 0){
  annotation <- annotation[!annotation$batch %in% fixed_batches, ]
  
  fixed_order <- fixed_order[match(fixed_batches, fixed_order$batch), ]
  annotation <- rbind(annotation, fixed_order)
}

cormat <- cormat[rownames(annotation),
                 rownames(annotation)]

pool_lvls <- unique(annotation$BioRep)
pool_colors <- RColorBrewer::brewer.pal(length(pool_lvls),
                                        'Set3')
names(pool_colors) <- pool_lvls

library(ComplexHeatmap)

rw_annot <- rowAnnotation(
  Stage = annotation$Stage
)
col_annot <- HeatmapAnnotation(
  Stage = anno_block(
    gp = gpar(fill = stage_colors$Stage_colour),
    labels = stage_colors$Stage
  ),
  
  'TissueID' = annotation$BioRep,
  
  'Chrom. vers.' = annotation$Chromium_Version,
  
  col = list(
    TissueID = pool_colors,
    'Chrom. vers.' = c(V2 = 'gold',
                       V3 = 'darkblue')
  )
)
row_annot <- rowAnnotation(
  Stage = anno_block(
    gp = gpar(fill = stage_colors$Stage_colour),
    labels = stage_colors$Stage
  )
)

pdf(plot_file, width = 15, height = 12)
Heatmap(
  cormat,
  cluster_rows = FALSE,
  cluster_columns = FALSE,
  column_labels = annotation$batch,
  row_labels = annotation$batch,
  
  column_split = annotation$Stage,
  column_title = NULL,
  row_split = annotation$Stage,
  row_title = NULL,

  top_annotation = col_annot,
  left_annotation = row_annot,
  
  name = 'Spearman\'s rho'
)
dev.off()


# pdf(plot_file, width = 7, height = 7)
# pheatmap(
#   cormat,
#   show_rownames = FALSE,
#   show_colnames = FALSE,
#   cluster_rows = FALSE,
#   cluster_cols = FALSE,
#   annotation_col = annotation
# )
# dev.off()
