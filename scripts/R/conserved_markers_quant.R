library(tidyverse)

species <- c(
    Human = "hum",
    Mouse = "mou",
    Opossum = "opo"
)

                                        # reading the TF-IDF output
tfidf_out <- lapply(species, function(s){
    read_csv(glue::glue("data/csv/gene_enrichments/{s}_tfidf_celltype_match.csv"))
})


                                        # reading metadata
gmd <- read_csv("data/metadata/all_genes_meta.csv") %>%
  filter(ortho_type == "ortholog_one2one") %>%
  group_by(ortho_id) %>%
  filter(n() == 3) %>%
  mutate(species = case_when(
           species=="HUM" ~ "Human",
           species=="MOU" ~ "Mouse",
           species=="OPO" ~ "Opossum"
         ))


                                        # find all detectable genes
use_genes <- lapply(species, function(s){
  rownames(readRDS(glue::glue("data/sce/updated/{s}_sce.rds")))
})

use_genes <- use_genes %>%
  map(function(x){
    x <- x[x%in% gmd$gene_id]
    gmd$ortho_id[match(x, gmd$gene_id)]
  })

use_genes <- purrr::reduce(use_genes, intersect)
                                        # subsetting for 1:1 x vs mouse
tfidf_out <- tfidf_out %>%
  map(function(x) x %>% filter(gene_id %in% gmd$gene_id))


df <- lapply(names(tfidf_out), function(s){
  tfidf_out[[s]] %>%
    add_column(species = s)
}) %>%
  do.call(rbind,.) %>%
  filter(qval < .01)

use_cts <- c("astrocyte",
             "progenitor",
             "NTZ_neuroblast",
             "VZ_neuroblast",
             "Purkinje",
             "GCP",
             "GC",
             "UBC",
             "oligo",
             "oligodendrocyte",
             "GABA_DN",
             "interneuron")

dev_order <- c("", "progenitor", "diff", "defined", "mature")
plot_order <- use_cts %>%
    map(function(x){
    paste(x, dev_order, sep="_") %>%
        str_remove(pattern = "_$")
    }) %>%
    do.call("c",.) %>%
    map(function(x){
    paste(x, c("","1","2","3"), sep="_") %>%
        str_remove(pattern = "_$")
    }) %>%
    do.call("c",.)

## filters for genes which are
## expressed in at least 5% of cells within a group
## and have a 2x enrichment
min_freq <- .01
min_enrichment_x <- 2
df_sub <- df %>%
  filter(str_detect(cluster, paste(use_cts, collapse = "|"))) %>%
  filter(geneFrequency > min_freq) %>%
  filter(geneFrequency / geneFrequencyOutsideCluster > min_enrichment_x)

plot_order <- plot_order[plot_order %in% df_sub$cluster]

## joining the orthology information
df_plot <- df_sub %>%
  select(gene_id, species, cluster, qval, geneFrequency, geneFrequencyOutsideCluster, tfidf) %>%
  left_join(gmd %>% select(gene_id, ortho_id, ortho_name)) %>%
  filter(ortho_id %in% use_genes)

## saving the output for GO term enrichment analysis:
write_csv(df_plot, "data/csv/markers_species_for_go.csv")
write_csv(tibble(useable_genes = use_genes), "data/csv/useable_genes_for_go.csv")

df_plot <- df_plot %>%
  select(ortho_name, species, qval, cluster) %>%
  group_by(cluster, ortho_name) %>%
  summarise(species = list(species)) %>%
  ungroup() %>%
  mutate(cluster_ordered = factor(cluster, levels = plot_order))

df %>% 
  filter(geneFrequency > min_freq) %>%
  filter(geneFrequency / geneFrequencyOutsideCluster > min_enrichment_x) %>% 
  arrange(cluster, species) %>%
  select(gene_id, species, cluster, qval, geneFrequency, geneFrequencyOutsideCluster, tfidf) %>%
  left_join(gmd %>% select(gene_id, ortho_id, ortho_name)) %>%
  filter(ortho_id %in% use_genes) %>% 
  select(ortho_name, species, qval, cluster) %>%
  group_by(cluster, ortho_name) %>%
  summarise(species = list(species)) %>%
  ungroup() %>%
  rowwise() %>%
  mutate(species = paste0(species, collapse="/")) %>%
  ungroup() %>%
  write_csv("data/csv/markers_species_grouped.csv")

library(ggupset)
plt <- df_plot %>%
  ggplot(aes(x = species)) +
  geom_bar() +
  scale_x_upset(order_by = "degree") +
  facet_wrap(vars(cluster_ordered), nrow=2)
ggsave("figures/final/conserved_marker_genes_upset.pdf",
       plot = plt,
       width=14,
       height=4)
