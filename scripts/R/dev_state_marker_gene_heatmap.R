suppressPackageStartupMessages({
  library(tidyverse)
  library(SingleCellExperiment)
})

species <- c(
  Mouse = 'mou',
  Human = 'hum',
  Opossum = 'opo'
)

# df <- species %>% 
#   map(function(s){
#     read_csv(glue::glue('data/csv/gene_enrichments/{s}_tfidf_celltype_match.csv')) %>% 
#       add_column(species = s)
#   }) %>% 
#   do.call(rbind,.)


df <- read_csv('data/csv/markers_species_for_go.csv')
scl <- readRDS('data/sce/full_orthologs_sce.rds')

color_df <- read_csv('data/metadata/color_codes_annotation.csv')
dev_state_colors_df <- color_df %>% 
  select(dev_state, dev_state_colour) %>% 
  distinct()
dev_state_colors <- dev_state_colors_df$dev_state_colour
names(dev_state_colors) <- dev_state_colors_df$dev_state

gene_names <- df %>% 
  select(ortho_id, ortho_name) %>% 
  distinct()

cpm <- scl %>% 
  map(function(s){
    g <- colData(s)[,'dev_state']
    u <- assay(s, 'umi')
    x <- tapply(1:ncol(u),
           g,
           function(x){
             # if(length(x) > 50){
             #   x <- sample(x, 50, replace = FALSE)
             # }
             rowSums(u[,x,drop=F])
           }) %>% 
      do.call(cbind,.)
    x <- x[rownames(x) %in% df$ortho_id, ]
    rownames(x) <- gene_names$ortho_name[match(rownames(x), 
                                               gene_names$ortho_id)]
    t(t(x) / colSums(x) * 1e6)
  })
use_groups <- cpm %>% 
  map(colnames) %>% 
  purrr::reduce(intersect)

cpm <- cpm %>% 
  map(function(x) x[,use_groups])


cpm_df <- lapply(names(cpm), function(n){
  cpm[[n]] %>% 
    reshape2::melt() %>% 
    as_tibble(.) %>% 
    add_column(species = n)
}) %>% 
  do.call(rbind,.) %>% 
  dplyr::rename(
    gene = Var1,
    group = Var2
  )

sub <- df %>% 
  mutate(enrich = (geneFrequency + .0001)/ (geneFrequencyOutsideCluster + .0001)) %>% 
  select(ortho_name, species, cluster, tfidf, enrich) %>% 
  group_by(ortho_name, cluster) %>% 
  summarise(gr = paste(species, collapse = '/'),
            tfidf = median(tfidf),
            enrich = median(enrich)) %>% 
  group_by(gr, cluster) %>% 
  top_n(20, enrich) %>% 
  arrange(cluster, gr, desc(tfidf))

theme_set(theme_classic())


target<-c("progenitor","glioblast", 
          "astrocyte", "oligo_progenitor",
          "oligodendrocyte", "VZ_neuroblast_1", 
          "VZ_neuroblast_2", "VZ_neuroblast_3", 
          "parabrachial", "GABA_DN_defined","Purkinje_diff", 
          "Purkinje_defined", "Purkinje_mature", "interneuron_diff", 
          "interneuron_defined", "NTZ_neuroblast_1",
          "NTZ_neuroblast_2", "NTZ_neuroblast_3",
          "glut_DN_defined", 
          "GCP","GCP/UBCP", "GC_diff_1", "GC_diff_2", 
          "GC_defined", "UBC_diff", "UBC_defined")

target <- target[target %in% unique(sub$cluster)]


sub <- sub %>% 
  ungroup() %>% 
  mutate(cluster = factor(cluster, 
                          levels = target)) %>% 
  arrange(cluster, gr)


gr_colors <- c(
  'black',
  '#E59638',
  '#50B3D1',
  '#7C5115',
  '#446775',
  '#B2A467',
  '#758C50'
)
names(gr_colors) <- c(
  'Mouse',
  'Human',
  'Opossum',
  'Human/Mouse',
  'Mouse/Opossum',
  'Human/Opossum',
  'Human/Mouse/Opossum'
)

cpm_df %>% 
  right_join(
    sub %>% 
      dplyr::rename(gene = ortho_name),
    by = 'gene'
  ) %>% 
  filter(group %in% unique(sub$cluster)) %>% 
  mutate(gene = factor(gene,
                       levels = unique(sub$ortho_name))) %>% 
  mutate(group = factor(group,
                        levels = rev(target))) %>% 
  group_by(species, gene) %>% 
  mutate(value = value - mean(value)) %>% 
  mutate(value = value / sd(value)) %>% 
  mutate(value = case_when(
    value < 0 ~ 0,
    value > 2 ~ 2,
    TRUE ~ value
  )) %>% 
    ggplot(aes(x = gene,
               y = group)) +
    geom_tile(aes(fill = gr,
                  alpha = value),
              size = .75) +
    facet_grid(vars(species), vars(gr),
               scales = 'free_x') +
    scale_fill_manual(values = gr_colors) +
    guides(fill = guide_none()) +
    labs(
      alpha = 'capped z-score\n[0 - 2]',
      y = 'dev_state'
    ) +
    theme(
      axis.text.x = element_blank(),
      axis.ticks.x = element_blank()
    )
ggsave('figures/final/marker_gene_heatmap_all.pdf',
       width = 18,
       height = 10)  

use_gr <- c(
  'Mouse specific',
  'Human specific',
  'Opossum specific',
  'Conserved'
)

facet_order <- c(
  'Mouse',
  'Human',
  'Opossum'
)

gr_colors <- c(
  'black',
  '#E58D00',
  '#50B3D1',
  '#758C50'
)
names(gr_colors) <- use_gr

cpm_df %>% 
  right_join(
    sub %>% 
      dplyr::rename(gene = ortho_name),
    by = 'gene'
  ) %>% 
  filter(group %in% unique(sub$cluster)) %>% 
  mutate(gene = factor(gene,
                       levels = unique(sub$ortho_name))) %>% 
  mutate(group = factor(group,
                        levels = rev(target))) %>% 
  group_by(species, gene) %>% 
  mutate(gr = case_when(gr == 'Human/Mouse/Opossum' ~ 'Conserved',
                        gr == 'Mouse' ~ 'Mouse specific',
                        gr == 'Human' ~ 'Human specific',
                        gr == 'Opossum' ~ 'Opossum specific',
                        TRUE ~ gr)) %>% 
  filter(gr %in% use_gr) %>% 
  mutate(gr = factor(gr, levels = use_gr)) %>% 
  mutate(value = value - mean(value)) %>% 
  mutate(value = value / sd(value)) %>% 
  mutate(value = case_when(
    value < 0 ~ 0,
    value > 2 ~ 2,
    TRUE ~ value
  )) %>% 
  ungroup() %>% 
  mutate(species = dplyr::recode(species,
    MOU = 'Mouse',
    HUM = 'Human',
    OPO = 'Opossum'
  )) %>% 
  mutate(species = factor(species, levels = facet_order)) %>% 
  # mutate(value = sqrt(value+1)) %>% 
    ggplot(aes(x = gene,
               y = group)) +
    geom_tile(aes(fill = gr,
                  alpha = value),
              size = .75) +
    # facet_grid(cols = vars(species)) +
    facet_grid(vars(species), vars(gr),
               scales = 'free_x') +
    # scale_fill_gradientn(colors = rev(viridis::inferno(100))) +
    scale_fill_manual(values = gr_colors) +
    scale_color_manual(values = dev_state_colors) +
    # scale_x_discrete(limits = genes_to_plot) +
    guides(fill = guide_none()) +
    labs(
      alpha = 'capped z-score\n[0 - 2]',
      y = 'dev_state'
    ) +
    theme(
      axis.text.x = element_blank(),
      axis.ticks.x = element_blank()
    )
ggsave('figures/final/marker_gene_heatmap.pdf',
       width = 15,
       height = 10)  
