library(tidyverse)
library(SingleCellExperiment)
#suppressMessages(source("scripts/R/sce_helper.R"))
color_list <- readRDS("data/metadata/color_list.rds")


species <- c("hum", "mou", "opo")
names(species) <- species

use_tissue <- c("cerebellum", "cerebellar cx")

# using only cerebellar cell types
subtype_cerebellar <- read_tsv('data/metadata/preciest_label_cerebellar.tsv')


## Reading the metadata from
## the SingleCellExperiment objects

md <- lapply(species, function(s){
  readRDS(glue::glue("data/sce/updated/{s}_sce.rds")) %>%
    colData() %>%
    as.data.frame() %>%
    rownames_to_column("cell_id") %>%
    as_tibble()
})

md_colnames <- lapply(md, colnames) %>%
  purrr::reduce(intersect)

md <- lapply(md, function(x) x[,md_colnames]) %>%
  do.call(rbind,.) %>%
  filter(Tissue %in% use_tissue) %>% 
  left_join(subtype_cerebellar)


## Now, using the aligned stages
## to align the data species specific time points
sm_df <- read_csv("data/metadata/stage_matching_colors_long.csv")

# plot_order <- c("mural/endoth","immune", "erythroid",
#                 "motorneuron", "MBO","GABA_MB", "MB_neuroblast",
#                 "isthmic_neuroblast", "neural_crest_progenitor",
#                 "meningeal", "GC/UBC", "UBC",
#                 "GC", "glut_DN", "isth_N",
#                 "NTZ_neuroblast", "NTZ_mixed", "interneuron", "Purkinje",
#                 "GABA_DN", "noradrenergic", "parabrachial",
#                 "VZ_neuroblast", "ependymal", "oligo",
#                 "astroglia")

md <- md %>%
  left_join(sm_df, by = c("Stage", "species"))

# contaminations <- c(
#   "isthmic_neuroblast", "MB_neuroblast", "GABA_MB", "motorneuron", "neural_crest_progenitor",
#   "ND", "NA", "immune", "MBO", "meningeal",
#   "GC/UBC", "NTZ_mixed", "noradrenergic", "ependymal"
# )

# plot_order <- plot_order[!plot_order %in% contaminations]

## Reframe the following human Carnegie stages to
## their corresponding wpc.
md <- md %>%
  mutate(
    Stage = case_when(
      Stage == "CS18" ~ "7 wpc",
      Stage == "CS19" ~ "7 wpc",
      Stage == "CS22" ~ "8 wpc",
      TRUE ~ Stage
    )
  ) %>%
  mutate(
    Stage = paste0(str_extract(Match_Stage, "^.*_"), Stage)
  ) %>%
  mutate(
    Stage = str_remove(Stage, "^NA")
  )

md <- md %>%
  filter(!is.na(is.cerebellar)) %>% 
  filter(is.cerebellar) %>% 
  group_by(species, Match_Stage, cell_type, TissueID, Capture.System) %>%
  dplyr::count() %>%
  filter(n >= 50) %>%
  group_by(species, Match_Stage, TissueID, Capture.System) %>%
  mutate(perc = n/sum(n) * 100) %>%
  ## mutate(cell_type = factor(cell_type, levels = plot_order)) %>%
  # filter(! cell_type %in% contaminations) %>%
  ungroup() %>%
  mutate(species = dplyr::recode(species,
                                 MOU = "Mouse",
                                 OPO = "Opossum",
                                 HUM = "Human"))


write_csv(md, "data/csv/summarise_ct_proportions.csv")
