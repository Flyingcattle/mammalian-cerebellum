suppressPackageStartupMessages({
  library(tidyverse)
  library(SingleCellExperiment)
})


## CMD ARGS ---------------------------------------------------------------------
args <- commandArgs(trailingOnly = TRUE)
sce_file <- args[1]
stopifnot(file.exists(sce_file))
sce <- readRDS(sce_file)


embedding <- args[2]
stopifnot(embedding %in% reducedDimNames(sce))


outfile <- args[3]

## MAIN -------------------------------------------------------------------------
m <- reducedDim(sce, embedding) %>%
  as.matrix() %>%
  as.data.frame()

colnames(m) <- paste0(embedding, "_", 1:ncol(m))

m <- m %>%
  rownames_to_column("cell_id") %>%
  as_tibble(.)

md <- colData(sce) %>%
  as.data.frame() %>%
  rownames_to_column("cell_id") %>%
  as_tibble(.)

md <- md %>%
  left_join(m, by = "cell_id")

write_csv(md, outfile)
