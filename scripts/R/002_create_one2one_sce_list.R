## source("scripts/R/sce_helper.R")
library(SingleCellExperiment)
library(tidyverse)
## theme_set(readRDS("data/metadata/custom_plot_theme.rds"))


species = c("HUM", "MOU", "OPO")

dat = lapply(species,function(s) readRDS(paste0("data/sce/updated/", 
                                                         tolower(s),"_sce.rds" )))
             
names(dat) = species

## Removing not expressed genes
dat <- dat %>%
  map(function(x){
    umi <- assay(x, 'umi')
    x <- x[rowSums(umi) > 0, ]
    return(x)
  })

## Removing human deep nuclei samples
## dat = lapply(dat, function(s) s[,!str_detect(s$Tissue, " dn$")])

genes.meta = read_csv("data/metadata/all_genes_meta.csv")

o2o.meta = genes.meta %>% filter(ortho_type == "ortholog_one2one")

#Subsetting the genes to all 1:1:1-orthologs and renaming the gene to the mouse gene name (for simplicity).

o2o = lapply(dat, function(s) {
    s = s[rownames(s) %in% o2o.meta$gene_id, ]
    rownames(s) = o2o.meta$ortho_id[match(rownames(s), o2o.meta$gene_id)]
    
    return(s)
})

o2o.genes = lapply(o2o, rownames) %>% purrr::reduce(intersect)

o2o = lapply(o2o, function(s) s[o2o.genes, ])

## o2o = lapply(o2o, sce.pca)

print(o2o)

saveRDS(o2o, "data/sce/full_orthologs_sce.rds")
