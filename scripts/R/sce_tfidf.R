#### CMD ARGUMENTS ####
library(optparse)

option_list = list(
  make_option(
    c("-g","--group_by"), 
    type="character", 
    default="cell_type", 
    help="Column in metadata to group the cell by [cell_type]", 
    metavar="character"
  ),

  make_option(
    c("-n","--n_out"), 
    type="numeric", 
    default=10000,
    help="Maximum number of genes reported per group [1000]", 
    metavar="integer"
  ),

  make_option(
    c("-s","--subset"),
    type="character",
    default=NULL,
    help="Column to subsample by",
    metavar="character"
  ),

  make_option(
    c("--subset_to"),
    type="numeric",
    default=1000,
    help="Subsample to n [1000]",
    metavar="integer"
  ),

  make_option(
    c("--FDR"), 
    type="numeric", 
    default=.05, 
    help="False discovery rate", 
    metavar="rational"
  )
); 
 
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser, positional_arguments = TRUE);

#### PARAMETERS ####
par_sce_file <- opt$args[1] #"data/sce/updated/mou_sce.rds"
par_group_by <- opt$options[["group_by"]] #"cell_type"
par_subset <- opt$options[["subset"]]
par_subset_to <- opt$options[["subset_to"]]
par_outfile <- opt$args[2] #"tmp/out.csv"
par_N <- opt$options[["n_out"]]  #1000
par_FDR <- opt$options[["FDR"]]  #.05


#### checks ####
stopifnot(file.exists(par_sce_file))

library(tidyverse)
suppressMessages(library(SingleCellExperiment))

sce <- readRDS(par_sce_file)

## if(par_subset == "Matched_Stage"){
##   used_match_file <- "data/metadata/shared_matched_stages.csv"
##   message("Adding the matched staging information.")
##   message("using: ", used_match_file)

##   matched_stages <- read_csv(used_match_file)
##   matched_stages <- matched_stages %>%
##     filter(species == unique(sce$species))

##   sce <- sce[,sce$Stage %in% matched_stages$Stage]
##   sce$Matched_Stage <- matched_stages$Match_Stage[match(sce$Stage, matched_stages$Stage)]
## }

stopifnot(par_group_by %in% colnames(colData(sce)))


####### FUNCTIONS #######
#' Gets top N markers for each cluster
#'
#' Uses tf-idf ordering to get the top N markers of each cluster.  For each cluster, either the top N or all genes passing the hypergeometric test with the FDR specified, whichever list is smallest.
#' 
#' Term Frequency - Inverse Document Frequency is used in natural language processing to identify terms specific to documents.  This function uses the same idea to order genes within a group by how predictive of that group they are.  The main advantage of this is that it is extremely fast and gives reasonable results.
#'
#' To do this, gene expression is binarised in each cell so each cell is either considered to express or not each gene.  That is, we replace the counts with \code{toc > zeroCut}.  The frequency with which a gene is expressed within the target group is compared to the global frequency to calculate the tf-idf score.  We also calculate a multiple hypothesis corrected p-value based on a hypergeometric test, but this is extremely permissive.
#'
#' @export
#' @param toc Table of counts.  Must be a sparse matrix.
#' @param clusters Vector of length \code{ncol(toc)} giving cluster membership.
#' @param N Number of marker genes to return per cluster.
#' @param FDR False discover rate to use. 
#' @param expressCut Value above which a gene is considered expressed.
#' @return data.frame with top N markers (or all that pass the hypergeometric test) and their statistics for each cluster.
#' @examples
#' #Calculate markers of clusters in toy data
#' mrks = quickMarkers(scToy$toc,scToy$metaData$clusters)
#' \dontrun{
#' #Calculate markers from Seurat (v3) object
#' mrks = quickMarkers(srat@assays$RNA@count,srat@active.ident)
#' }              
quickMarkers = function(toc,clusters,N=10,FDR=0.01,expressCut=0.9){
  #Convert to the more manipulable format
  toc = as(toc,'dgTMatrix')
  w = which(toc@x>expressCut)
  #Get the counts in each cluster
  clCnts = table(clusters)
  nObs = split(factor(rownames(toc))[toc@i[w]+1],clusters[toc@j[w]+1])
  nObs = sapply(nObs,table)
  #Calculate the observed and total frequency
  nTot = rowSums(nObs)
  tf = t(t(nObs)/as.integer(clCnts[colnames(nObs)]))
  ntf = t(t(nTot - nObs)/as.integer(ncol(toc)-clCnts[colnames(nObs)]))
  idf = log(ncol(toc)/nTot)
  score = tf*idf
  #Calculate p-values
  qvals = lapply(seq_len(ncol(nObs)),function(e)
                 p.adjust(phyper(nObs[,e]-1,nTot,ncol(toc)-nTot,clCnts[colnames(nObs)[e]],lower.tail=FALSE),method='BH'))
  qvals = do.call(cbind,qvals)
  colnames(qvals) = colnames(nObs)
  #Get gene frequency of second best
  sndBest = lapply(seq_len(ncol(tf)),function(e) apply(tf[,-e,drop=FALSE],1,max))
  sndBest = do.call(cbind,sndBest)
  colnames(sndBest) = colnames(tf)
  #And the name
  sndBestName = lapply(seq_len(ncol(tf)),function(e) (colnames(tf)[-e])[apply(tf[,-e,drop=FALSE],1,which.max)])
  sndBestName = do.call(cbind,sndBestName)
  colnames(sndBestName) = colnames(tf)
  rownames(sndBestName) = rownames(tf)
  #Now get the top N for each group
  w = lapply(seq_len(ncol(nObs)),function(e){
             o = order(score[,e],decreasing=TRUE)
             if(sum(qvals[,e]<FDR)>=N){
               o[seq(N)]
             }else{
               o[qvals[o,e]<FDR]
             }
                 })
  #Now construct the data.frame with everything
  ww = cbind(unlist(w,use.names=FALSE),rep(seq_len(ncol(nObs)),lengths(w)))
  out = data.frame(gene_id = rownames(nObs)[ww[,1]],
                   cluster = colnames(nObs)[ww[,2]],
                   geneFrequency = tf[ww],
                   geneFrequencyOutsideCluster = ntf[ww],
                   geneFrequencySecondBest = sndBest[ww],
                   geneFrequencyGlobal = nTot[ww[,1]]/ncol(toc),
                   secondBestClusterName = sndBestName[ww],
                   tfidf = score[ww],
                   idf = idf[ww[,1]],
                   qval = qvals[ww],
                   stringsAsFactors=FALSE)
  return(out)
}
                       
sce.tfidf = function(sce, group_col = "cell_type", N = 10, FDR = .01, expressCut = .9){
    stopifnot(group_col  %in% colnames(colData(sce)))
    
    X = assay(sce, "umi")
    gr = colData(sce)[,group_col]
    
    quickMarkers(X, gr, N = N, FDR = FDR, expressCut = expressCut)
}



#### MAIN ####

if(!is.null(par_subset)){
  cat("Subsampling by", par_subset, "\n")
  i <- 1:ncol(sce)
  g <- paste0(sce[[par_subset]], "//",sce[[par_group_by]])

  use_cells <- tapply(i, g, function(y){
    set.seed(1); sample(y, size = min(length(y), par_subset_to))
  }) %>%
    do.call("c", .)

  sce <- sce[,use_cells]
  print(sce)

}

# running TF-IDF on the sce object
tfidf_out <- sce.tfidf(
  sce = sce,
  group_col = par_group_by,
  N = par_N,
  FDR = par_FDR,
  expressCut = .9
) %>% 
  as_tibble()

write_csv(
  tfidf_out,
  path = par_outfile
)
