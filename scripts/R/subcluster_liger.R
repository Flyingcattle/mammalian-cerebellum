library(Matrix)
library(tidyverse)
library(liger)
source("https://gist.githubusercontent.com/klprint/1ab4468eb3c54abcf0422dec6223b8fc/raw/96624adfe2f611b7c6568806d54c8062420d172c/single_cell_functions.R")
theme_set(theme_gray())

args <- commandArgs(trailingOnly = T)

# args <- c("generated_data/liger_output/mou_liger_out.rds",
#           "generated_data/liger_derived/mou_liger_cluster_coarse.csv",
#           "test", "10")

liger_file <- args[1]
liger_clustered <- args[2]
outfolder <- args[3]
n.threads <- as.numeric(args[4])

mou.liger <- readRDS(liger_file)
cmd <- read_csv(liger_clustered)


run.liger <- function(sub.umi, min.cells = 10, k_liger = 25){
  
  sub.umi.mtx <- do.call(merge.sparse, sub.umi)
  
  k <- min(min(do.call("c", lapply(sub.umi, ncol)))-1,
           k_liger)
  
  
  sub.liger <- createLiger(sub.umi)
  sub.liger <- liger::normalize(sub.liger)
  sub.liger <- selectGenes(sub.liger, do.plot = F)
  sub.liger <- scaleNotCenter(sub.liger)
  sub.liger <- optimizeALS(sub.liger, k)
  sub.liger.nnmf <- do.call(rbind, sub.liger@H)
  set.seed(1234)
  sub.liger.umap <- uwot::umap(sub.liger.nnmf, metric = "cosine", min_dist=.1, n_neighbors = 30, n_components = 3, verbose = F)
  
  set.seed(1234)
  sub.liger.umap2d <- uwot::umap(sub.liger.nnmf, metric = "cosine", min_dist=.1, n_neighbors = 30, n_components = 2, verbose = F)
  rownames(sub.liger.umap) <- rownames(sub.liger.nnmf)
  
  sub.cluster <- scanpy.louvain(sub.umi, sub.liger.nnmf, resolution = 1)
  
  plot.factor.3d(sub.liger.umap, qsplit(rownames(sub.liger.nnmf), 3), .2)
  plot.factor.3d(sub.liger.umap, sub.cluster, .2)
  
  sub.meta.data <- tibble(
    cell_id = rownames(sub.liger.nnmf),
    cluster = as.character(sub.cluster),
    UMAP1 = sub.liger.umap2d[,1],
    UMAP2 = sub.liger.umap2d[,2]
  ) %>%
    separate(cell_id, c("batch", "species", "stage", "bc"), "_", remove = F)
  
  
  sub.meta.data %>%
    ggplot(aes(x = UMAP1, y = UMAP2, color = stage)) +
    geom_point(size=.1)
  
  sub.meta.data <- sub.meta.data %>% dplyr::rename(sub_cluster = cluster)
  sub.meta.data <- sub.meta.data %>%
    mutate(sub_cluster = sub_cluster)
  
  return(
    list(
      meta.data = sub.meta.data,
      liger = sub.liger.nnmf,
      umap2d = sub.liger.umap2d,
      umap3d = sub.liger.umap,
      umi = sub.umi.mtx
    )
  )
}

cell.groups <- lapply(sort(unique(cmd$cluster)), function(cl){
  keep.cells <- cmd  %>%
    filter(cluster == cl) %>%
    pull(cell_id)
})
names(cell.groups) <- paste0("cl", as.character(sort(unique(cmd$cluster))))

input.umi <- lapply(cell.groups, function(kc){
  sub.umi <- lapply(mou.liger@raw.data, function(x){
    if(sum(colnames(x) %in% kc) > 10){
      return(x[,colnames(x) %in% kc])
    }else{
      return(NA)
    }
    
  })
  
  sub.umi <- sub.umi[!is.na(sub.umi)]
  
  print(length(sub.umi))
  
  if(length(sub.umi) < 2) return(NA)
  
  return(sub.umi)
})

batch.process <- cut(1:length(input.umi), ceiling(length(input.umi) / 5))

tmp.list <- list()
for( b in levels(batch.process)){
  print(b)
  tmp.umi <- input.umi[batch.process == b]
  tmp.list[[b]] <- pbmcapply::pbmclapply(tmp.umi, function(i.umi){
    if(is.na(i.umi)) return(NA)
    out <- try(run.liger(i.umi))
    
    if(inherits(out, "try-error")){
      y <- i.umi[i.umi %>% map(ncol) %>% do.call("c",.) > 25]
      if(length(y) < 2) out <- NA
      out <- run.liger(y)
    }
    
    return(out)
    
  }, mc.cores = n.threads, ignore.interactive = T)
  
}

tmp <- do.call("c", tmp.list)


names(tmp) <- names(input.umi)


cat("Processing the results\n")

for(x in names(tmp[!is.na(tmp)])){
  md <- tmp[[x]]$meta.data
  
  md <- md %>% left_join(cmd) %>% 
    select(cell_id, batch, species, stage, bc, cluster, sub_cluster, UMAP1, UMAP2) %>% 
    mutate(sub_cluster = paste0("orig.cl_", cluster, "_", sub_cluster))
  
  tmp[[x]]$meta.data <- md
}

saveRDS(tmp, outfolder)