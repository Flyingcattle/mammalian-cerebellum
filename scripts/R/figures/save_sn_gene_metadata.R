library(SingleCellExperiment)
library(tidyverse)

sce <- lapply(c(
  hum = 'hum',
  mou = 'mou',
  opo = 'opo'
), function(s){
  readRDS(glue::glue('data/sce/updated/{s}_sce.rds'))
})



a <- lapply(sce, function(s){
  s <- s[rownames(s) %in% gmd$gene_id, ]
  
  present_genes <- gmd$ortho_id[match(rownames(s),
                                      gmd$gene_id)]
  
  out <- tibble(
    mouse_id = present_genes
  )
  
  out[[glue::glue("{unique(s$species)}_gene_id")]] <- rownames(s)
  return(out)
})


dir.create('data/csv/sn_gene_lists', showWarnings = FALSE, recursive = T)
a %>% 
  purrr::reduce(inner_join) %>% 
  write_csv('data/csv/sn_gene_lists/shared_orthologs.csv')


sce %>% 
  map(function(s){
    tibble(
      gene_id = rownames(s)
    ) %>% 
      write_csv(glue::glue('data/csv/sn_gene_lists/{unique(s$species)}_genes.csv'))
  })

